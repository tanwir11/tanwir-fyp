#include <stdio.h>
#include <stdlib.h>
#include <math.h>

const int nkpts = 4000;


extern "C" void no_zero(int *xarray, int *yarray, int *x1array, int *y1array)
{
	int k=0;

	for(int i=0; i<nkpts; i++)
	{
		if((xarray[i] && yarray[i])!=0)
		{
			x1array[k] = xarray[i];
			y1array[k] = yarray[i];
			
			//printf("%d	%d	%d\n", x1array[k], y1array[k],k);
			k=k+1;
		}
	//printf("%d	%d	%d\n", x1array[i], y1array[i],i);
	
	}
}



extern "C" void merge_cord (int *xarray, int *yarray, int *new_array, int val)
{	

	int y=0;

	for(int i=0; i<val; i++)
	{
		new_array[y] = xarray[i];
		new_array[y+1] = yarray[i];
		//printf("%d	%d\n", new_array[y], new_array[y+1]);
		y=y+2;
	}
	//printf("///////////////////////////////////////////////////////////////////////////////////////");
}


extern "C" void nine_points(double *xy, double xl, double xr, double yl, double yr)
{

	xy[0] = xl*xr;
	xy[1] = yl*xr;
	xy[2] = xr;
	xy[3] = xl*yr;
	xy[4] = yl*yr;
	xy[5] = yr;
	xy[6] = xl;
	xy[7] = yl;
	xy[8] = 1;

	for(int i=0; i<9; i++)
	{
		//printf("%f  ", xy[i]);
	}
//printf("\n");



}







////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


extern "C" void normalize_coordinates( int *curr_x, int *curr_y, int *prev_x, int *prev_y, double* leftx, double *rightx, double *lefty, double *righty, double *trans_mat )
{

	int curr_sumx=0, curr_sumy=0, prev_sumx=0, prev_sumy=0 ;
	int currx_tr[8];
	int curry_tr[8];
	int prevx_tr[8];
	int prevy_tr[8];
	//for(int hh = 0; hh<10; hh++)
	{	printf("currx	curry	prevx	prevy\n");
		for(int kk=0; kk<8; kk++)
		{
			label:  int v = rand() % (5000 + 1 - 0) + 0; 
				if(curr_x[v]==0)
				{	
					goto label; 
				}
				currx_tr[kk] = curr_x[v];
				curry_tr[kk] = curr_y[v];
				prevx_tr[kk] = prev_x[v];
				prevy_tr[kk] = prev_y[v];
				curr_sumx = curr_sumx + curr_x[v]; curr_sumy = curr_sumy + curr_y[v]; prev_sumx = prev_sumx + prev_x[v]; prev_sumy = prev_sumy + prev_y[v];
				
				printf("%d	%d 	%d	%d\n",currx_tr[kk], curry_tr[kk],prevx_tr[kk], prevy_tr[kk]);
				


		}

		//printf("\n");
	}


	double curr_centroidx = curr_sumx/(double)8;
	double curr_centroidy = curr_sumy/(double)8;
	double prev_centroidx = prev_sumx/(double)8;
	double prev_centroidy = prev_sumy/(double)8;



	

	double curr_d=0, prev_d=0;

	//cout<<"sample"<<" "<<"centroid"<<" "<<" diff"<<endl;
	for (int vv =0; vv<8; vv++)
	{
		double diff_curr_x = currx_tr[vv] - curr_centroidx; double diff_curr_y = curry_tr[vv] - curr_centroidy;
		double diff_curr_x_squared  = diff_curr_x* diff_curr_x; double diff_curr_y_squared = diff_curr_y * diff_curr_y;
		curr_d = sqrt((double)diff_curr_x_squared + (double)diff_curr_y_squared) + curr_d;

		double diff_prev_x = prevx_tr[vv] - prev_centroidx; double diff_prev_y = prevy_tr[vv] - prev_centroidy;
		double diff_prev_x_squared  = diff_prev_x * diff_prev_x; double diff_prev_y_squared = diff_prev_y * diff_prev_y;
		prev_d = sqrt((double)diff_prev_x_squared + (double)diff_prev_y_squared) + prev_d;



	}	

	double avg_prev_dist_to_centroid = prev_d/8;
	double avg_curr_dist_to_centroid = curr_d/8;

	trans_mat[0] = curr_centroidx;
	trans_mat[1] = curr_centroidy;
	trans_mat[2] = avg_curr_dist_to_centroid;
	trans_mat[3] = prev_centroidx;
	trans_mat[4] = prev_centroidy;
	trans_mat[5] = avg_prev_dist_to_centroid ;


	//cout<<" centroid"<<endl;
	//cout<<curr_centroidx<<" "<<curr_centroidy<<" "<<prev_centroidx<<" "<<prev_centroidy<<endl;
	//cout<<prev_dist_to_centroid<<" "<<curr_dist_to_centroid<<endl;


	//cout<<"normalized"<<endl;
	//cout<<"normalized_leftx"<<"  "<<"original_left_x"<<"  "<<"centroid_x"<<endl;

	for(int yy=0; yy<8; yy++)
	{

		leftx[yy] = (double)(currx_tr[yy] - curr_centroidx)/avg_curr_dist_to_centroid ; lefty[yy] = (double)(curry_tr[yy] - curr_centroidy)/avg_curr_dist_to_centroid;
		rightx[yy] = (double)( prevx_tr[yy] - prev_centroidx )/avg_prev_dist_to_centroid;  righty[yy] = (double)( prevy_tr[yy] - prev_centroidy )/avg_prev_dist_to_centroid; 
		//cout<<leftx[yy]<<" 		"<<currx_tr[yy]<<"		 "<<curr_centroidx<<endl;
		//cout<<leftx[yy]<<" "<<lefty[yy]<<" "<<rightx[yy]<<' '<<righty[yy]<< endl;

	}



}






//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////







extern "C" void matrix_A(double *A, double* leftx, double *rightx, double *lefty, double *righty )
{
	//printf("printing matrix A\n");
	int w=8, h=9; 
	for(int x=0; x<w; x++ )
	{
		double *xy = (double *) malloc(sizeof(double)*9);
		nine_points(xy, leftx[x], rightx[x], lefty[x], righty[x]);
		for(int y=0; y<h; y++)
		{
			A[y*w + x] = xy[y];
			//printf("%f  ", A[y*w +x]);

		}
		
		//printf("\n \n");
		free(xy);

	}

}





