#include <stdio.h>

extern "C" void no_zero(int *xarray, int *yarray, int *x1array, int *y1array);
extern "C" void merge_cord (int *xarray, int *yarray, int *new_array, int val);
extern "C" void nine_points(double *xy, double xl, double xr, double yl, double yr);
extern "C" void matrix_A(double *A, double* leftx, double *rightx, double *lefty, double *righty );
extern "C" void normalize_coordinates( int *curr_x, int *curr_y, int *prev_x, int *prev_y, double* leftx, double *rightx, double *lefty, double *righty, double *trans_mat );
