#include <cv.h>
#include <highgui.h>
#include <iomanip>
#include <iostream>
#include "opencv2/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/opencv.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <sys/timeb.h>
#include <time.h>
#include <cuda_runtime_api.h>
#include <cuda.h>
#include <stdio.h>
#include <stdint.h>
#include<assert.h>
#include <string>
#include <sstream>
#include <cstdlib>
#include <cblas.h>
#include <lapacke.h>

using namespace std;

typedef double value;

/** Column major style! */
string mat2string(int m, int n, value* A)
{
  ostringstream oss;
  for (int j=0;j<m;j++)
  {
    for (int k=0;k<n;k++)
    {
      oss << A[j+k*m] << "\t";
    }
    oss << endl;
  }
  return oss.str();
}




extern "C" void transpose(double *array, int w, int h)
{
    double new_array[w * h];
    for (int x = 0; x < w; x++)
    {
        for (int y = 0; y < h; y++)
        {
            int old_idx = y * w + x;
            int new_idx = x * h + y;
            new_array[new_idx] = array[old_idx];
        }
    }
    for (int i = 0; i < h * w; i++)
    {
        array[i] = new_array[i];
    }
}





extern "C" void svd_fund(double *fund, double *right_H, double* left_H, double *cam_out)
{
  //> Part 1. Decomposition. -----------------------------------------
  char jobu  = 'A'; // Return the complete matrix U
  char jobvt = 'A'; // Return the complete matrix VT
  int mA = 3;
  int nA = 3;
  int lda = 3;
  int ldu = 3;
  int ldvt = 3;
  int lwork = 5*3;
  int info = 0;
  value* result = (value*)malloc(mA*nA*sizeof(value));
  value* U = (value*)malloc(mA*mA*sizeof(value));
  value* VT = (value*)malloc(nA*nA*sizeof(value));
  value* Svec = (value*)malloc(3*sizeof(value));
  value* work = (value*)malloc(3*3*sizeof(value));



  std::cout << "Matrix A (will be overwritten, as is documented):" << std::endl <<
    mat2string(mA,nA,fund);

  // Citing lapacke.h
  //lapack_int LAPACKE_dgesvd(int matrix_order, char jobu, char jobvt,
  //   lapack_int m, lapack_int n, double* a,
  //   lapack_int lda, double* s, double* u, lapack_int ldu,
  //   double* vt, lapack_int ldvt, double* superb);

  info = LAPACKE_dgesvd(LAPACK_COL_MAJOR, jobu, jobvt, mA, nA, fund, lda, Svec, U, ldu, VT, ldvt, work);
  std::cout << "Ran dgesvd. Let's see ..." << std::endl <<
    "unit_fund_mat U:" << std::endl << mat2string(mA,mA,U) <<
    "unit_fund_mat Svec:" << std::endl << mat2string(1,nA,Svec) <<
    "VT:" << std::endl << mat2string(nA,nA,VT) <<
    "Info Code: " << info << std::endl << std::endl <<
    "All is well." << std::endl;
     Svec[nA-1] = 0;
  //< ----------------------------------------------------------------
  //> Part 2. Checking the result. -----------------------------------
  value* S = (value*)malloc(mA*nA*sizeof(value));

	int x=0;
	int y=0;

  for( x=0; x<3; x++)
  {
	for( y=0; y<3; y++)
	{
		if(y==x)
		{
			S[y*3+x] = Svec[x];

		}
		else{S[y*3+x] = 0;}
		

	}

	

  }

cout<<" SSSS: "<<endl<<mat2string(3,3,S)<<endl;


  value* cons = (value*)malloc(mA*nA*sizeof(value));
  
  // Citing cblas.h
  // void cblas_dgemm(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
  //   const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
  //   const int K, const double alpha, const double *A,
  //   const int lda, const double *B, const int ldb,
  //   const double beta, double *C, const int ldc);

  // work := S*VT; (2x3)=(2x3)*(3x3)
  cblas_dgemm(CblasColMajor,CblasNoTrans,CblasNoTrans,3,3,3,1.0,S,3,VT,3,0.0,cons,3);
  cout << " unit_fund_mat Step 1: S*VT" << endl << mat2string(3,3,cons);

  // A := U*work; (2x2)*(2x3)
  cblas_dgemm(CblasColMajor,CblasNoTrans,CblasNoTrans,3,3,3,1.0,U,3,cons,3,0.0,result,3);
  cout << "unit_fund_mat := U*S*VT:" << endl << mat2string(3,3,result) << endl;
  //< ----------------------------------------------------------------


  /// step  1 = fund_mat*left_H;

double *fund_1 = (double *) malloc (sizeof(double)*3*3);


  cblas_dgemm(CblasColMajor,CblasNoTrans,CblasNoTrans,3,3,3,1.0,result,3,left_H,3,0.0,fund_1,3);
  cout<<"left_H:"<<endl<<mat2string(3,3,left_H) << endl;
  cout << "unit_fund_mat step 1 := fund*left_H:" << endl << mat2string(3,3,fund_1) << endl;

double *denorm_mat = (double *) malloc (sizeof(double)*3*3);
 cout<<"right_H:"<<endl<<mat2string(3,3,right_H) << endl;

  cblas_dgemm(CblasColMajor,CblasNoTrans,CblasNoTrans,3,3,3,1.0,right_H,3,fund_1,3,0.0,denorm_mat,3);
  cout << "final constructed mat:" << endl << mat2string(3,3,denorm_mat) << endl;

	memcpy(cam_out, denorm_mat, sizeof(double)*3*3 );


  cout << "cam_fund_mat:" << endl << mat2string(3,3,cam_out) << endl;


  free(result); free(U); free(VT); free(Svec); free(work); free(S); free(cons); free(denorm_mat); free(fund_1);
 
}



extern "C" void homo(double *A, int x,int y)
{
	A[0] =(double)x; A[1] =(double)y; A[2] = (double)1;
}



extern "C" void mat_mult(double *A, double *B, int a_m, int a_n, int b_m, int b_n, double *C )

{

  // Citing cblas.h
  // void cblas_dgemm(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
  //   const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
  //   const int K, const double alpha, const double *A,
  //   const int lda, const double *B, const int ldb,
  //   const double beta, double *C, const int ldc);


	double *result = (double *) malloc(sizeof(double)*a_m*b_n);
	//cblas_dgemm(CblasColMajor,CblasNoTrans,CblasNoTrans,3,3,3,1.0,right_H,3,fund_1,3,0.0,denorm_mat,3);
	cblas_dgemm(CblasColMajor,CblasNoTrans,CblasNoTrans, a_m, b_n, a_n, 1.0, A, a_m, B, b_m, 0.0,result,a_m);
	memcpy(C,result, a_m*b_n*sizeof(double));
	//cout << "A matrix:" << endl << mat2string(a_m,a_n,A) << endl;
	//cout << "B matrix:" << endl << mat2string(b_m,b_n,B) << endl;
 	//cout << "result matrix:" << endl << mat2string(a_m,b_n,C) << endl;

	free(result);
}



extern "C" double total_error (int *leftx, int *lefty, int *rightx, int *righty, int nkpts, double *cam_fund_mat)
{
	double total_score =0; double threshold = 0.001; double score =0; double error =0;
	for (int i=0; i<nkpts; i++)

	{

		if(leftx[i]!=0 && lefty[i] !=0 && rightx[i]!=0 && leftx[i]!=0)
		{
			double *A_left = (double *)malloc(sizeof(double)*3);
			double *A_right = (double *)malloc(sizeof(double)*3);

			homo( A_left, leftx[i], lefty[i]);
			homo(A_right, rightx[i], righty[i]);

			transpose(A_right, 3, 1);

			double *result_1 = (double *)malloc(sizeof(double)*3*1);
			double *single_error = (double *)malloc(sizeof(double)*1*1);
 
			mat_mult(cam_fund_mat, A_left, 3, 3, 3, 1, result_1 );
			mat_mult(A_right, result_1, 1, 3, 3, 1, single_error);
			
			if(abs(single_error[0]) >= threshold)
			{
				score =0;
			}
			else if(abs(single_error[0]) < threshold)
			{
				score = 1 - (single_error[0]*single_error[0])/(threshold*threshold); 
			}
			
			total_score = total_score + score;
			error = error + single_error[0];
			cout<<"single error:"<<single_error[0]<<endl;

			free(A_left);
			free(A_right);
			free(result_1);
			free(single_error);
			



		} //end of if !=0


	} //end of for

	cout<<"error:"<<error<<endl;
	return total_score;



}






















