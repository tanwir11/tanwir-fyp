#include <cv.h>
#include <highgui.h>
#include <iomanip>
#include <iostream>
#include "opencv2/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/opencv.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <sys/timeb.h>
#include <time.h>
#include <cuda_runtime_api.h>
#include <cuda.h>
#include <stdio.h>
#include <stdint.h>
#include<assert.h>



















#include <string>
#include <sstream>
#include <cstdlib>
#include <cblas.h>
#include <lapacke.h>

using namespace std;

typedef double value;

/** Column major style! */
string matrix2string(int m, int n, value* A)
{
  ostringstream oss;
  for (int j=0;j<m;j++)
  {
    for (int k=0;k<n;k++)
    {
      oss << A[k*m+j] << "\t";
    }
    oss << endl;
  }
  return oss.str();
}

extern "C" void svd_dd(double *A, int m, int n, double *unit_fund_mat)
{
  //> Part 1. Decomposition. -----------------------------------------
  char jobu  = 'A'; // Return the complete matrix U
  char jobvt = 'A'; // Return the complete matrix VT
  int mA = m;
  int nA = n;
  int lda = m;
  int ldu = m;
  int ldvt = n;
  int lwork = 5*min(m,n);
  int info = 0;
  value* result = (value*)malloc(mA*nA*sizeof(value));
  value* U = (value*)malloc(mA*mA*sizeof(value));
  value* VT = (value*)malloc(nA*nA*sizeof(value));
  value* Svec = (value*)malloc(n*sizeof(value));
  value* work = (value*)malloc(lwork*sizeof(value));

  //A[0] = 1; A[2] = 2; A[4] = 4;
  //A[1] = 0; A[3] = 0; A[5] = 4;

  std::cout << "Matrix A (will be overwritten, as is documented):" << std::endl <<
    matrix2string(mA,nA,A);

  // Citing lapacke.h
  //lapack_int LAPACKE_dgesvd(int matrix_order, char jobu, char jobvt,
  //   lapack_int m, lapack_int n, double* a,
  //   lapack_int lda, double* s, double* u, lapack_int ldu,
  //   double* vt, lapack_int ldvt, double* superb);

  info = LAPACKE_dgesvd(LAPACK_COL_MAJOR, jobu, jobvt, mA, nA, A, lda, Svec, U, ldu, VT, ldvt, work);
  std::cout << "Ran dgesvd. Let's see ..." << std::endl <<
    "U:" << std::endl << matrix2string(mA,mA,U) <<
    "Svec:" << std::endl << matrix2string(1,nA,Svec) <<
    "VT:" << std::endl << matrix2string(nA,nA,VT) <<
    "Info Code: " << info << std::endl << std::endl <<
    "All is well." << std::endl;

double min_val = 25;
int min_index =0;

for(int j=0; j<n; j++)
{

	if(Svec[j]<min_val)
	{
		min_val = Svec[j];
		min_index = j;


	}


}


cout<<"min_val"<<" "<<min_val<<" "<<"min_index"<<" "<<min_index<<endl;
cout<<" min_vecotr"<<endl;

double *min_vector = (double *)malloc(n*sizeof(double));




for(int k =0; k<n; k++)
{
	min_vector[k] = VT[n*k+min_index];

	cout<<min_vector[k]<<endl;
	

}


int kk=0;
double *first_fund_mat = (double *)malloc(9*sizeof(double));
double sum[3] = {0};
double max_sum =0;

for(int w =0; w<3; w++ )
{

	for(int h=0; h<3; h++)
	{
		first_fund_mat[h*3+w] = (min_vector[kk]);
		sum[w] =  sum[w] + abs(min_vector[kk]);
		kk++;
	}

	if(max_sum<sum[w])
	{
		max_sum = sum[w];
	}

}




for(int c=0; c<3; c++)
{

	for(int d=0; d<3; d++)
	{
		unit_fund_mat[d*3+c] = first_fund_mat[d*3+c]/max_sum;

	}


}




std::cout<<"first_fund_mat:"<<std::endl<<matrix2string( 3, 3, first_fund_mat)<<std::endl;
std::cout<<"absolute sums:"<<std::endl<<matrix2string( 3, 1, sum)<<std::endl;
std::cout<<"max_sum"<<": "<<max_sum<<endl;
std::cout<<"unit_fund_mat:"<<std::endl<<matrix2string( 3, 3, unit_fund_mat)<<std::endl;



free(first_fund_mat);
free(min_vector);



	
  //< ----------------------------------------------------------------
  //> Part 2. Checking the result. -----------------------------------
  value* S = (value*)malloc(mA*nA*sizeof(value));
	int x=0;
	int y=0;

  for( x=0; x<m; x++)
  {
	for( y=0; y<n; y++)
	{
		if(y==x)
		{
			S[y*m+x] = Svec[x];

		}
		else{S[y*m+x] = 0;}
		if(x==m-1 && y==n-1){S[y*m+x] = 1;}

	}

	

  }


std::cout<<"S:"<<std::endl<<matrix2string( m, n, S)<<std::endl;

double *svt = (double *)malloc(sizeof(double)*n*m);






  // Citing cblas.h
  /* void cblas_dgemm(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
     const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
     const int K, const double alpha, const double *A,
     const int lda, const double *B, const int ldb,
    const double beta, double *C, const int ldc);*/

  // work := S*VT; (2x3)=(2x3)*(3x3)
  cblas_dgemm(CblasColMajor,CblasNoTrans,CblasNoTrans,mA,nA,nA,1.0,S,lda,VT,ldvt,1.0,svt,lda)    ;
  cout << "Step 1: S*VT" << endl << matrix2string(m,n,svt);
printf("\n \n");

  // A := U*work; (2x2)*(2x3)
  cblas_dgemm(CblasColMajor,CblasNoTrans,CblasNoTrans,mA,nA,mA,1,U,ldu,svt,lda,1.0,result,lda);
  cout << "A := U*S*VT:" << endl << matrix2string(mA,nA,result) << endl;
  //< ----------------------------------------------------------------
  free(result);

	 
   free(Svec);  free(U);  free(VT);  free(work);  cout<<"we have freed"<<endl;

   free(svt);free(S);
  //return EXIT_SUCCESS;
}






















