CXX=g++

CUDA_INSTALL_PATH=/usr/local/cuda-8.0
CFLAGS= -I. -I$(CUDA_INSTALL_PATH)/include `pkg-config --cflags opencv`
LDFLAGS= -L$(CUDA_INSTALL_PATH)/lib64 -lcudart`pkg-config --libs opencv`

#Uncomment the line below if you dont have CUDA enabled GPU
#EMU=-deviceemu
#$(CXX) Debug/main.o `pkg-config --libs opencv` -L$(CUDA_INSTALL_PATH)/lib64 -lcudart -o Debug/grayscale
#$(CXX) Debug/main.o `pkg-config --libs opencv` -L$(CUDA_INSTALL_PATH)/lib64 -lcudart Debug/fast.o -o Debug/grayscale
#$(CXX) Debug/main.o `pkg-config --libs opencv` -L$(CUDA_INSTALL_PATH)/lib64 -lcudart Debug/fast.o Debug/brief.o -o Debug/grayscale
#$(CXX) $(CFLAGS) -c arrays.cpp -o Debug/arrays.o
ifdef EMU
CUDAFLAGS+=-deviceemu
endif

all:
				

	$(CXX) $(CFLAGS) -c main.cpp -o Debug/main.o 
	nvcc $(CUDAFLAGS) -c fast.cu -o Debug/fast.o -Wno-deprecated-gpu-targets
	nvcc $(CUDAFLAGS) -c brief.cu -o Debug/brief.o -Wno-deprecated-gpu-targets
	nvcc $(CUDAFLAGS) -c region_match.cu -o Debug/region_match.o -Wno-deprecated-gpu-targets
	$(CXX) $(CFLAGS) -c arrays.cpp -o Debug/arrays.o
	$(CXX) Debug/main.o `pkg-config --libs opencv` -L$(CUDA_INSTALL_PATH)/lib64 -lcudart  Debug/fast.o Debug/brief.o Debug/region_match.o Debug/arrays.o -o Debug/grayscale

	

clean:
	rm -f Debug/*.o Debug/grayscale


#-L usr/local/cuda/lib64/libcudart.so.8.0 
#-L usr/local/cuda-8.0/lib64/libcudart.so.8.0 
#$(CXX) $(CFLAGS) -c test1.cpp -o Debug/test1.o `pkg-config --libs opencv`






