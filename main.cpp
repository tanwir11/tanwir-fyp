
#include "fast.h"
#include "brief.h"
#include "region_match.h"
#include "arrays.h"
#include "svd_demo.h"
#include "fund_mat_svd.h"
#include <cv.h>
#include <highgui.h>
#include <iomanip>
#include <iostream>
#include "opencv2/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/opencv.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <sys/timeb.h>
#include <time.h>
#include <cuda_runtime_api.h>
#include <cuda.h>
#include <stdio.h>
#include <stdint.h>
#include<assert.h>

using namespace cv;
using namespace std;


const int nkpts = 5000;
int offy1;
int offx1;
int offy2;
int offx2;
int frame_count = 0;







#define DEBUG_TIME



#define cudaCheckErrors(msg) \
    do { \
        cudaError_t __err = cudaGetLastError(); \
        if (__err != cudaSuccess) { \
            fprintf(stderr, "Fatal error: %s (%s at %s:%d)\n", \
                msg, cudaGetErrorString(__err), \
                __FILE__, __LINE__); \
            fprintf(stderr, "*** FAILED - ABORTING\n"); \
            exit(1); \
        } \
    } while (0)

int offsetx1[256] = {-91,109,-44,74,-124,-59,58,110,-60,43,-98,28,27,20,-120,10,-85,-62,36,18,-127,61,4,29,-33,-12,5,-65,-27,-45,29,5,22,-65,78,-53,-2,66,9,48,13,60,28,36,-19,-43,39,-128,-25,11,-10,27,-105,-56,13,-52,12,-33,-54,-17,52,55,-9,8,36,-4,38,-19,-78,-38,51,12,97,-24,2,-66,22,77,-9,-77,60,4,-12,-67,-35,-10,42,-35,-3,39,-14,-30,13,-11,-46,5,-9,7,12,-80,19,-46,34,37,-50,22,-60,-21,127,7,-9,-38,50,31,108,-19,16,-41,11,-90,0,-23,-48,23,55,34,17,-128,-104,-65,-51,-35,-42,-28,61,-35,-20,37,-12,8,50,5,-2,-67,34,45,-3,-47,64,-13,99,36,-57,6,23,22,-50,-33,2,-73,20,74,0,-25,-63,50,-16,-39,26,19,-43,-23,30,-48,-35,76,19,0,42,27,-57,20,12,47,-36,-10,25,42,63,-18,22,23,-1,-74,-90,-33,17,57,4,30,-26,74,-29,75,21,-71,-35,118,-7,-4,2,-103,-50,-31,-14,-30,-65,-23,-91,45,37,95,-124,-72,6,-26,-5,19,-9,-10,-111,-9,-89,-48,69,20,-26,13,9,-74,13,44,-5,-75,-114,90,127,-69,-9,58,-2,-86,37,2,102,-47};

int offsetx2[256] = {11,38,-32,-10,8,15,-65,10,4,72,-52,1,91,-46,-42,-45,40,61,20,9,29,-41,61,-52,27,0,20,-36,-27,-48,28,-27,-29,14,-17,101,-39,-4,11,22,-66,6,-17,-10,42,87,42,-17,52,47,-15,-29,-52,35,15,66,-8,-6,23,-12,18,2,-47,-56,-45,-15,9,-11,67,35,25,-13,-56,-20,73,83,-51,27,-15,-24,-11,-33,-74,42,-8,17,-46,-42,-26,26,-76,47,1,50,-47,45,78,31,-5,21,55,-47,-1,-71,-95,-38,-50,53,-65,-112,16,69,-12,-19,-30,-34,34,80,14,-66,1,25,1,-27,19,50,-59,30,-61,-79,-21,72,71,38,10,-26,-59,-21,-128,-34,21,-10,41,18,101,-53,-48,-1,-3,-15,-30,127,115,-71,35,-12,1,116,30,93,27,-84,48,-50,-2,48,-11,-11,116,91,36,7,38,-13,115,-7,-36,1,-4,30,-73,74,-65,-11,127,49,-19,-12,-26,-128,89,-55,26,26,64,34,24,46,2,42,47,83,83,15,67,-38,-26,3,-94,-20,-7,-9,-73,-71,16,14,-30,-23,-45,-15,-31,32,26,2,6,-34,-93,55,3,-39,46,-25,-39,-14,63,53,41,0,37,23,43,3,52,40,33,-10,-2,52,-11,25,-15,64,-4,5,75,48};

int offsety1[256] = {61,41,63,9,-52,104,127,-20,-24,5,30,-54,21,51,-32,36,-64,27,2,33,39,68,66,-10,-47,109,-11,17,5,63,28,-9,-89,-2,-64,-19,43,-13,11,37,37,-7,47,-80,41,-26,57,12,26,-41,-30,-27,-13,42,32,-20,-1,-24,52,62,20,18,61,63,45,-42,-90,-24,-50,-25,-6,-18,101,-5,0,-39,-27,31,-17,-72,-22,26,-19,65,-27,45,20,6,-25,108,79,-32,-64,50,31,-26,0,36,30,-2,24,72,15,-4,11,88,-28,41,84,-5,-51,1,91,93,53,-6,9,127,-67,18,62,63,74,-25,0,39,-116,-25,49,95,56,76,-54,-31,29,13,15,34,15,40,-115,-38,18,-75,-12,16,-38,16,-11,-22,-30,8,-36,39,31,-17,23,127,-61,41,24,5,14,-50,8,-72,46,34,55,9,69,-9,109,71,-28,7,12,8,-4,71,-9,43,103,-36,-6,32,40,-52,-33,-94,-23,-49,59,-16,-128,7,49,17,59,12,-8,50,1,-38,-78,-60,-28,47,-12,49,41,44,48,-48,-10,-24,55,-20,75,11,-87,54,-58,11,-65,-29,-16,41,-87,-33,-30,38,7,100,-2,31,-71,-26,7,6,12,-3,-22,13,-32,18,112,-128,-29,12,82,0,-28,-54,-26,-2};


int offsety2[256] = {16,-46,-18,40,85,28,-84,45,-108,-59,-22,-9,8,-4,53,-53,23,-28,-7,18,8,-5,42,-1,-9,-37,67,83,21,23,-90,60,57,-56,-62,-11,56,-71,70,50,-60,5,-7,-128,81,59,-17,51,-47,-11,75,127,4,-97,1,104,-66,92,34,45,6,-9,8,-64,-68,78,109,0,24,-32,53,38,-19,-33,78,0,30,-18,17,25,-26,-27,-9,-27,-62,49,-13,98,-11,-27,55,19,-77,8,-33,-7,-55,38,-17,15,-28,80,-24,17,59,43,-48,-78,-15,-1,21,-22,42,-37,-24,77,91,-31,63,-7,21,-38,108,-94,61,-30,77,22,-67,-32,52,-99,86,-33,14,-11,20,-25,-38,23,69,74,66,16,56,69,-18,-6,83,51,67,127,16,15,-25,6,-128,34,-27,-17,41,-71,-33,-33,-74,-44,-54,50,-5,110,51,30,71,-27,-38,2,-8,4,73,-4,35,-20,-107,-63,58,53,-47,-21,83,21,-105,-30,8,35,106,-56,54,-20,-81,6,3,1,-35,-82,-48,-39,8,38,13,-29,-53,2,12,-29,25,-90,0,-29,-19,50,-16,-78,32,43,-7,-33,-38,11,73,42,56,-5,-99,55,55,72,-36,-12,-43,33,36,12,73,-27,26,-49,-80,-50,-29,-63,-32,68,-8,-65,29,3};






int main(int argc, char** argv)
{
	


	char* imageName = argv[1];
	VideoCapture capture(imageName);
	Mat color_image, input_image;
	int frame=0;
	int h, w;
	int frame_back,frame_back_count;
	int numpixels = 0;
	//int c[1];
	int m[1];
	const dim3 cy;
	int key_id = 0;
	int count_realloc_gl = 1;	
	int count_realloc_match = 0;
	int count_realloc_gl_back = 0;
	int *matched_id = (int *)malloc(1*sizeof(int));
	int *gl_id = (int *)malloc(1*sizeof(int));
	memset(matched_id,0,1*sizeof(int));
	memset(gl_id,0,1*sizeof(int));
	int desc_count = 0;
	int val;
	int flag_copy_matchcord_to_curr =0;
	int *prev_match = (int *)malloc(nkpts*sizeof(int));
	int *curr_match = (int *)malloc(nkpts*sizeof(int));
	memset(curr_match,0,nkpts*sizeof(int));
	memset(prev_match,0,nkpts*sizeof(int));
	int draw_flag =0;




	vector<struct desc> gl_descriptor;




	int *prev_x = (int *) malloc(nkpts*sizeof(int));
	if(prev_x == NULL)
	{
		cout<<"failed to allocate prev_x"<<endl;
		return -1;
	}

	int *prev_y = (int *) malloc(nkpts*sizeof(int));
	if(prev_y == NULL)
	{
		cout<<"failed to allocate prev_y"<<endl;
		return -1;
	}

	int *curr_x = (int *) malloc(nkpts*sizeof(int));
	if(curr_x == NULL)
	{
		cout<<"failed to allocate prev_x"<<endl;
		return -1;
	}

	int *curr_y = (int *) malloc(nkpts*sizeof(int));
	if(curr_y == NULL)
	{
		cout<<"failed to allocate prev_y"<<endl;
		return -1;
	}

	memset(curr_x,0,nkpts*sizeof(int));
	memset(curr_y,0,nkpts*sizeof(int));
	memset(prev_x,0,nkpts*sizeof(int));
	memset(prev_y,0,nkpts*sizeof(int));


	int dummy;


	int size; 

	int gl_counter = 0;
	int realloc_flag = 0;
 

   	if( !capture.isOpened() )
	{
        throw "Error when reading steam_avi";	
	}

	namedWindow( "wi", 1);

	struct desc *gl_desc = (struct desc*) malloc(1*sizeof(struct desc));
	memset(gl_desc,0,1*sizeof(struct desc));

	int *gl_x = (int *) malloc(1*sizeof(int));
	int *gl_y = (int *) malloc(1*sizeof(int));
	memset(gl_x, 0, 1*sizeof(int));
	memset(gl_y, 0, 1*sizeof(int));

	int unique_id = 0;


	
	

	for(frame =0 ; frame<11;frame++)
	{


		capture >> color_image;
		//color_image = imread(argv[1], CV_LOAD_IMAGE_UNCHANGED);
		cvtColor(color_image, input_image, CV_BGR2GRAY);
		if(input_image.empty())
		{
		    break;
		}

		int *c  = (int *)malloc(1*sizeof(int)); //c[0] = 0; // to be used in memcpy for atomic add
		memset(c,0,1*sizeof(int));

		
		w= input_image.cols; //rows
		h= input_image.rows; //cols
		numpixels = h*w;
		size = sizeof(unsigned char)*numpixels;

		unsigned char *h_in = (unsigned char *)malloc(sizeof(unsigned char)*h*w);
		if(h_in == NULL)
		{
			cout<<"h_in not malloced"<<" "<<frame<<endl;
			return -1;
		}

		for (int y=0; y<h; y++) //allocating memory to h_in
		{
			for (int x=0; x<w; x++)
			{	h_in[y*w+x] = input_image.at<uchar>(y,x);
				//cout<<(int)frame.at<uchar>(x,y);
				//cout<<(int)frame.at<uchar>(x,y)<<" "<< (int)h_in[y*w+x]<<endl;
			} // end for accessing columns
				//printf("\n"); printf("\n");printf("\n");
		}  //end for accesing rows


		unsigned char *h_outptr = (unsigned char *)malloc(size); //  memory pointer to array of BRIGHT pixels in host
		if(h_outptr == NULL)
		{
			cout<<"h_outptr not malloced"<<" "<<frame<<endl;
			return -1;
		}
 
		unsigned char *h_doutptr = (unsigned char *)malloc(size); // memory  pointer to array ofDARK pixels in host
		if(h_doutptr == NULL)
		{
			cout<<"h_doutptr not malloced"<<" "<<frame<<endl;
			return -1;
		}

		int *xcord = (int *)(malloc(nkpts*sizeof(int)));  // pointer to array of x coordinates for host output
		if(xcord == NULL)
		{
			cout<<"xcord not malloced"<<" "<<frame<<endl;
			return -1;
		}

		int *ycord = (int *)(malloc(nkpts*sizeof(int)));  // pointer to array of y coordinates for host output
		if(ycord == NULL)
		{
			cout<<"ycord not malloced"<<" "<<frame<<endl;
			return -1;
		}


		int *frame_match_x = (int *) malloc(1*sizeof(int));
		if(frame_match_x == NULL)
		{
			cout<<"frame_match_x not malloced"<<" "<<frame<<endl;
			return -1;
		}


		int *frame_match_y = (int *) malloc(1*sizeof(int));
		if(frame_match_y == NULL)
		{
			cout<<"frame_match_x not malloced"<<" "<<frame<<endl;
			return -1;
		}


		int *d_xcord;	 
		int *d_ycord;
		int *d_count;  // to be used in atomic add
		unsigned char *d_in;
		unsigned char *d_outptr;
		unsigned char *d_doutptr;


		memset(xcord,0,nkpts*sizeof(int));
		memset(ycord,0,nkpts*sizeof(int));
		memset(h_doutptr,0,nkpts*sizeof(unsigned char));
		memset(h_outptr,0,nkpts*sizeof(unsigned char));



		/////////////////////  allocating memory for fast kernel  ///////////////////////////
	

		cudaError_t cuda_err = cudaMalloc((void**) &d_outptr,size);
	
		if(cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMalloc d_outptr" << endl;
			return -1;
		} 


		cuda_err = cudaMalloc((void**) &d_in,size);
	
		if(cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMalloc d_in" << endl;
			return -1;
		} 

		
		cuda_err = cudaMalloc((void**) &d_doutptr,size);

		if(cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMalloc d_outptr2" << endl;
			return -1;
		}


		cuda_err = cudaMalloc((void**)&d_xcord,nkpts*sizeof(int));
	
		if(cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMalloc d_xcord" << endl;
			return -1;
		}

		cudaError_t error;

		/*cudaMemset((void**)&d_xcord, 0, nkpts*sizeof(int));
		cudaError_t error = cudaGetLastError();
		if (error != cudaSuccess) {
  		fprintf(stderr, "ERROR: %s \n", cudaGetErrorString(error));
		return
		}*/



		cuda_err = cudaMalloc((void**)&d_ycord,nkpts*sizeof(int));

		if(cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMalloc d_ycord" << endl;
			return -1;
		}


		cuda_err = cudaMalloc((void**)&d_count,1*sizeof(int));

		if(cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMalloc d_count" << endl;
			return -1;
		}
		////////////////////// end of allocation for fast kernel ///////////////////////

		//////////////////// transferring data from cpu to gpu for fast////////////////

		cuda_err = cudaMemcpy(d_in,h_in,size,cudaMemcpyHostToDevice);

		if (cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMemcpy d_in" << endl;
			return -1;
	    	}


		cuda_err = cudaMemcpy(d_outptr,h_outptr,size,cudaMemcpyHostToDevice);

		if (cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMemcpy d_outptr" << endl;
			return -1;
	    	}

		cuda_err = cudaMemcpy(d_doutptr,h_doutptr,size,cudaMemcpyHostToDevice);

		if (cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMemcpy d_doutptr" << endl;
			return -1;
	    	}



		cuda_err = cudaMemcpy(&d_count[0],&c[0],1*sizeof(int),cudaMemcpyHostToDevice);

		if (cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMemcpy d_count" << endl;
			return -1;
	    	}



		cuda_err = cudaMemcpy(d_xcord,xcord,nkpts*sizeof(int),cudaMemcpyHostToDevice);

		if (cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMemcpy d_xcord" << endl;
			return -1;
	    	}


		cuda_err = cudaMemcpy(d_ycord,ycord,nkpts*sizeof(int),cudaMemcpyHostToDevice);

		if (cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMemcpy d_ycord" << endl;
			return -1;
	    	}


	

		////////// end of transferring memory from cpu to gpu for fast////////////////



		//////// setting blocklength/////////////////////////////////////////////////
		int blocklength = 8;
		const dim3 blockSize(blocklength,blocklength,1);
		const dim3 gridSize(1+w/blocklength,1+h/blocklength,1);
		//const dim3 gridSize(1+100,1+100,1);

		const dim3 match_blockSize(blocklength,blocklength,1);
		const dim3 match_gridSize(1+nkpts/blocklength,1+nkpts/blocklength,1);


		//////////////launching fast kernel//////////////////////////////////////////

		cuda_iscorner(d_in, d_count, d_xcord, d_ycord, w, h, d_outptr, d_doutptr, gridSize, blockSize);
		cudaDeviceSynchronize();
		/*cudaError_t error = cudaGetLastError();
		if (error != cudaSuccess) {
  		fprintf(stderr, "ERROR: %s \n", cudaGetErrorString(error));
		}*/


		//////////////////// transferring memory from gpu to cpu after fast kernel ///

	    	cuda_err = cudaMemcpy(h_outptr,d_outptr,size,cudaMemcpyDeviceToHost);  // copying BRIGHT pixels to host

		if (cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMemcpy h_outptr" << endl;
			return -1;
	    	}

		cuda_err = cudaMemcpy(h_doutptr,d_doutptr,size,cudaMemcpyDeviceToHost); // copying DARK pixels to host

		if (cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMemcpy h_doutptr" << endl;
			return -1;
	    	}

		cuda_err = cudaMemcpy(&c[0],&d_count[0],1*sizeof(int),cudaMemcpyDeviceToHost);

		if (cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMemcpy count" <<endl;
			return -1;
	    	}

		cuda_err = cudaMemcpy(xcord,d_xcord,nkpts*sizeof(int),cudaMemcpyDeviceToHost);

		if (cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMemcpy xcord" << endl;
			return -1;
	    	}

		cuda_err = cudaMemcpy(ycord,d_ycord,nkpts*sizeof(int),cudaMemcpyDeviceToHost);

		if (cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMemcpy ycord" << endl;
			return -1;
	    	}






		////freeing memory in device ////////////////////////
		cuda_err = cudaFree(d_in);

		if(cuda_err != cudaSuccess)
		{
			cout<<"ERROR: Failed cudaFree d_in"<<endl;
			return -1;
		}


	 	cuda_err = cudaFree(d_outptr);

		if(cuda_err != cudaSuccess)
		{
			cout<<"ERROR: Failed cudaFree d_outptr"<<endl;
			return -1;
		}


		cuda_err = cudaFree(d_doutptr);

		if(cuda_err != cudaSuccess)
		{
			cout<<"ERROR: Failed cudaFree d_in"<<endl;
			return -1;
		}


		cuda_err = cudaFree(d_xcord);
	
		if(cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaFree d_xcord" << endl;
			return -1;
		}


		cuda_err = cudaFree(d_ycord);
	
		if(cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaFree d_ycord" << endl;
			return -1;
		}


		cuda_err = cudaFree(d_count);
	
		if(cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaFree d_count" << endl;
			return -1;
		}




		//////////////////////// allocating and declaring memory for brief kernel ///////////////////////////////
		struct desc *b_my_desc;
		int *b_xcord;	 
		int *b_ycord;
		unsigned char *b_in;

		struct desc *h_my_desc = (struct desc*) malloc(nkpts*sizeof(struct desc));
		if(h_my_desc == NULL)
		{
			cout<<"h_my_desc malloc failed"<<endl;
			return -1;
		}
		memset(h_my_desc, 0, nkpts*sizeof(struct desc));


		cuda_err = cudaMalloc((void**)&b_my_desc,nkpts*sizeof(struct desc));
		if(cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMalloc d_my_desc" << endl;
			return -1;
		}


		cuda_err = cudaMalloc((void**) &b_in,size);
		if(cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMalloc d_in" << endl;
			return -1;
		} 


		cuda_err = cudaMalloc((void**)&b_xcord,nkpts*sizeof(int));
		if(cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMalloc b_xcord" << endl;
			return -1;
		}


		cuda_err = cudaMalloc((void**)&b_ycord,nkpts*sizeof(int));
		if(cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMalloc b_ycord" << endl;
			return -1;
		}


		//////////////transferring memory from cpu to gpu for breif kernel ///////////////////////////////////

		cuda_err = cudaMemcpy(b_my_desc, h_my_desc, nkpts*sizeof(struct desc), cudaMemcpyHostToDevice);
		if (cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMemcpy d_my_desc" << endl;
			return -1;
	    	}


		cuda_err = cudaMemcpy(b_in,h_in,size,cudaMemcpyHostToDevice);
		if (cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMemcpy d_in" << endl;
			return -1;
	    	}

		cuda_err = cudaMemcpy(b_xcord,xcord,nkpts*sizeof(int),cudaMemcpyHostToDevice);
		if (cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMemcpy d_xcord" << endl;
			return -1;
	    	}


		cuda_err = cudaMemcpy(b_ycord,ycord,nkpts*sizeof(int),cudaMemcpyHostToDevice);
		if (cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMemcpy d_ycord" << endl;
			return -1;
	    	}






		const dim3 b_blockSize(blocklength,1,1);
		const dim3 b_gridSize(nkpts/blocklength,1,1);

		/////launching breif kernel///////////////////////////////////////////////////////////////////////////////
		cuda_descriptor(b_in, b_xcord, b_ycord, w, b_my_desc, b_gridSize, b_blockSize);
		cudaDeviceSynchronize();



		/////////////////transferring memory after brief kernel////////////////////////////////////////////////////

		cuda_err = cudaMemcpy(h_my_desc, b_my_desc, nkpts*sizeof(struct desc), cudaMemcpyDeviceToHost);
		if (cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaMemcpy h_my_desc" << endl;
			return -1;
	    	}



		/////free memory after brief ////////
		cuda_err = cudaFree(b_my_desc);
		if(cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaFree d_count" << endl;
			return -1;
		}

		cuda_err = cudaFree(b_in);	
		if(cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaFree d_count" << endl;
			return -1;
		}


		cuda_err = cudaFree(b_xcord);	
		if(cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaFree d_count" << endl;
			return -1;
		}


		cuda_err = cudaFree(b_ycord);	
		if(cuda_err != cudaSuccess)
		{
			cout << "ERROR: Failed cudaFree d_count" << endl;
			return -1;
		}

		error = cudaGetLastError();
		if (error != cudaSuccess) {
  		fprintf(stderr, "ERROR: %s \n", cudaGetErrorString(error));
		cout<<"after freeing b_ycord and frame ="<<frame<<endl;
		return -1;
		}





		for(int i=0;i<nkpts;i++)
		{
			if(xcord[i]!=0 && ycord[i]!=0)
			{
				//cout<<xcord[i]<<" "<<ycord[i]<<" "<<frame<<" "<<c[0]<<" "<<i<<endl;
				//cout<<xcord[i]<<" "<<ycord[i]<<" "<<c[0]<<endl;
				circle(color_image, Point(xcord[i],ycord[i]), 3, Scalar(0,255,0),2,0);
				for(int j=0; j<8; j++)
				{	//if(h_my_desc[i].my_bits[j]!=0)
					//{cout<<h_my_desc[i].my_bits[j];}	
				}
				//printf("\n");
				
			}
		}



		////// reallocating global stroages ////////
		
		struct desc *tmp3 = (struct desc *) realloc(gl_desc, nkpts*(frame+1)*sizeof(struct desc));
		if(tmp3 == NULL)
		{
			printf("gl_desc realloc failed\n");
			return -1;

		}
		else
		{
			gl_desc = tmp3;

		}


		int *tmp1 = (int *) realloc(gl_x, nkpts*(frame+1)*sizeof(int));
		if(tmp1 == NULL)
		{
			printf("gl_x realloc failed\n");
			return -1;

		}
		else
		{
			gl_x = tmp1;
		}
			

		int *tmp2 = (int *) realloc(gl_y, nkpts*(frame+1)*sizeof(int));
		if(tmp1 == NULL)
		{
			printf("gl_y failed\n");
			return -1;
		}
		else
		{
			gl_y = tmp2;
		}

		int *tmp7 = (int *) realloc(gl_id, (nkpts*(frame+1))*sizeof(int));
		if(tmp7 == NULL)
		{
			printf("gl_id realloc failed\n");
			return -1;		
		}
		else
		{
			gl_id = tmp7;
		}

		int *tmp8 = (int *) realloc(matched_id, (nkpts*(frame+1))*sizeof(int));
		if(tmp8 == NULL)
		{
			printf("gl_id realloc failed\n");
			return -1;
		}
		else
		{
			matched_id = tmp8;
		}






		//memcpy(gl_y + (frame)*nkpts, ycord, nkpts*sizeof(int));
		//memcpy(gl_x + (frame)*nkpts, xcord, nkpts*sizeof(int));
		//memcpy(gl_desc + (frame)*nkpts, h_my_desc, nkpts*sizeof(struct desc));

					
				/*for(int ff=frame*nkpts; ff<(frame+1)*nkpts; ff++)
				{	if(gl_x[ff]!=0 && gl_y[ff]!=0)
					{
						cout<<gl_x[ff]<<" "<<gl_y[ff]<<endl;
						for(int sd=0; sd<8; sd++)
						{
							//if(gl_desc[ff].my_bits[sd]!=0)
							{cout<<gl_desc[ff].my_bits[sd]<<" "<< h_my_desc[ff-(frame*nkpts)].my_bits[sd]<<endl;}
						}

					}

				}*/
			



		/////// finish storing pseudo global descriptor ////////




			if(frame == 0)
			{
			
				//memset(matched_id,0,nkpts*sizeof(int));



				for(int f=0; f<nkpts ;f++)
				{
					if((xcord[f] && ycord[f]) !=0)
					{
						unique_id=unique_id+1;		 
						gl_id[f] = unique_id;
						//prev_match[f] = gl_id[f]; 
						//cout<<gl_id[f]<<endl;
						//for(int hi=0;hi<256;hi++)
						{
								//printf("%d",gl_desc[f].my_bits[hi]);

						}
							//printf("\n");
							//cout<<gl_id[f]<<endl;
							//printf("\n");
							//printf("\n");
				  	}
				 	else{gl_id[f] = 0;}
				}

				memcpy(gl_y + (frame)*nkpts, ycord, nkpts*sizeof(int));
				memcpy(gl_x + (frame)*nkpts, xcord, nkpts*sizeof(int));
				memcpy(gl_desc + (frame)*nkpts, h_my_desc, nkpts*sizeof(struct desc));

			}





		frame_count =0;		
		frame_back=frame;

		if(frame>0)
		{	
			//for(int t=(frame-1); t<frame; t++)
			while(frame_back>0)
			{
					frame_count++;

					int *tmp4 = (int *) realloc(frame_match_x, nkpts*(frame_count)*sizeof(int));
					if(tmp4 == NULL)
					{
						printf("frame_match_x realloc failed\n");
						return -1;

					}
					else
					{
						frame_match_x = tmp4;

					}


					int *tmp5 = (int *) realloc(frame_match_y, nkpts*(frame_count)*sizeof(int));
					if(tmp4 == NULL)
					{
						printf("  frame_match_y realloc failed\n");
						return -1;

					}
					else
					{
						frame_match_y = tmp5;

					}
					struct desc *gl_desc_tr = (struct desc*) malloc(nkpts*sizeof(struct desc));
					memset(gl_desc_tr, 0, nkpts*sizeof(struct desc));
					if(gl_desc_tr == NULL)
					{
						cout<<"gl_desc_tr malloc failed"<<endl; return -1;				
					}
					memcpy(gl_desc_tr, (frame_back-1)*nkpts+gl_desc , nkpts*sizeof(struct desc));



					int *prev_x_tr = (int *) malloc(nkpts*sizeof(int));
					memset(prev_x_tr, 0, nkpts*sizeof(int));
					if(prev_x_tr == NULL)
					{
						cout<<"prev_x_tr malloc failed"<<endl; return -1;				
					}
					memcpy(prev_x_tr, (frame_back-1)*nkpts+gl_x , nkpts*sizeof(int));



					int *prev_y_tr = (int *) malloc(nkpts*sizeof(int));
					memset(prev_y_tr, 0, nkpts*sizeof(int));
					if(prev_y_tr == NULL)
					{
						cout<<"prev_y_tr malloc failed"<<endl; return -1;				
					}
					memcpy(prev_y_tr, (frame_back-1)*nkpts+gl_y , nkpts*sizeof(int));





					//// allocation of memory for matcher kernel //////
					int *m_xcord;	 
					int *m_ycord;
					int *m_matchx;
					int *m_matchy;
					int *m_prev_xcord;	 
					int *m_prev_ycord;
					int *m_prev_matchx;
					int *m_prev_matchy;
					int *m_id;
					int *m_matched_id;
					int *m_count;
					struct desc *m_old_desc;
					struct desc *m_new_desc;
					m[0] = 0;





					cuda_err = cudaMalloc((void**)&m_old_desc,nkpts*sizeof(struct desc));
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMalloc m_old_desc" << endl;
						return -1;
					}

					cuda_err = cudaMalloc((void**)&m_new_desc,nkpts*sizeof(struct desc));
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMalloc m_new_desc" << endl;
						return -1;
					}

					cuda_err = cudaMalloc((void**)&m_xcord,nkpts*sizeof(int));
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMalloc m_xcord" << endl;
						return -1;
					}

					cuda_err = cudaMalloc((void**)&m_ycord,nkpts*sizeof(int));
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMalloc m_ycord" << endl;
						return -1;
					}


					cuda_err = cudaMalloc((void**)&m_matchx,nkpts*sizeof(int));
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMalloc m_matchx" << endl;
						return -1;
					}


					cuda_err = cudaMalloc((void**)&m_matchy,nkpts*sizeof(int));
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMalloc m_matchy" << endl;
						return -1;
					}


					cuda_err = cudaMalloc((void**)&m_prev_xcord,nkpts*sizeof(int));
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMalloc m_prev_xcord" << endl;
						return -1;
					}




					cuda_err = cudaMalloc((void**)&m_prev_ycord,nkpts*sizeof(int));
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMalloc m_prev_ycord" << endl;
						return -1;
					}



					cuda_err = cudaMalloc((void**)&m_prev_matchx,nkpts*sizeof(int));
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMalloc prev_matchx" << endl;
						return -1;
					}


					cuda_err = cudaMalloc((void**)&m_prev_matchy,nkpts*sizeof(int));
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMalloc prev_matchy" << endl;
						return -1;
					}

					////////transferring memory from cpu to gpu for matcher kernel ///////////////////////

					cuda_err = cudaMemcpy(m_new_desc, h_my_desc, nkpts*sizeof(struct desc), cudaMemcpyHostToDevice);
					if (cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMemcpy d_my_desc" << endl;
						return -1;
				    	}


					cuda_err = cudaMemcpy(m_old_desc, gl_desc_tr, nkpts*sizeof(struct desc), cudaMemcpyHostToDevice);
					if (cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMemcpy m_old_desc" << endl;
						return -1;
				    	}


					cuda_err = cudaMemcpy(m_xcord,xcord,nkpts*sizeof(int),cudaMemcpyHostToDevice);
					if (cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMemcpy m_xcord" << endl;
						return -1;
				    	}


					cuda_err = cudaMemcpy(m_ycord,ycord,nkpts*sizeof(int),cudaMemcpyHostToDevice);
					if (cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMemcpy m_ycord" << endl;
						return -1;
				    	}

					cuda_err = cudaMemcpy(m_prev_xcord,prev_x_tr,nkpts*sizeof(int),cudaMemcpyHostToDevice);

					if (cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMemcpy m_prev_xcord" << endl;
						return -1;
				    	}

					cuda_err = cudaMemcpy(m_prev_ycord,prev_y_tr,nkpts*sizeof(int),cudaMemcpyHostToDevice);

					if (cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMemcpy m_prev_ycord" << endl;
						return -1;
				    	}





				
					////launching matcher kernel/////
					cuda_region_match(m_old_desc, m_new_desc, m_xcord,  m_ycord, m_matchx, m_matchy, m_prev_xcord, m_prev_ycord, m_prev_matchx, m_prev_matchy,  h , w , b_gridSize, 					b_blockSize);


					////transfer memory after matcher kernel///////////
					int *h_matchx = (int *)malloc(nkpts*sizeof(int));
					int *h_matchy = (int *)malloc(nkpts*sizeof(int));
					int *h_prev_matchx = (int *)malloc(nkpts*sizeof(int));
					int *h_prev_matchy = (int *)malloc(nkpts*sizeof(int));
					memset(h_matchx,0,nkpts*sizeof(int));
					memset(h_matchy,0,nkpts*sizeof(int));
					memset(h_prev_matchx,0,nkpts*sizeof(int));
					memset(h_prev_matchy,0,nkpts*sizeof(int));


					cuda_err = cudaMemcpy(h_matchx,m_matchx,nkpts*sizeof(int),cudaMemcpyDeviceToHost);
					if (cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMemcpy h_matchx" << endl;
						return -1;
				    	}

					cuda_err = cudaMemcpy(h_matchy,m_matchy,nkpts*sizeof(int),cudaMemcpyDeviceToHost);
					if (cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMemcpy h_matchy" << endl;
						return -1;
				    	}


					cuda_err = cudaMemcpy(h_prev_matchx,m_prev_matchx,nkpts*sizeof(int),cudaMemcpyDeviceToHost);
					if (cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMemcpy h_prev_matchx" << endl;
						return -1;
				    	}

					cuda_err = cudaMemcpy(h_prev_matchy,m_prev_matchy,nkpts*sizeof(int),cudaMemcpyDeviceToHost);
					if (cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaMemcpy h_prev_matchy" << endl;
						return -1;
				    	}







																		
					/////////// freeing memory after matcher kernel //////////////////////
					cuda_err = cudaFree(m_old_desc);
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaFree m_old_desc" << endl;
						return -1;
					}

					cuda_err = cudaFree(m_new_desc);
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaFree m_new_desc" << endl;
						return -1;
					}

					cuda_err = cudaFree(m_xcord);
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaFree m_xcord" << endl;
						return -1;
					}

					cuda_err = cudaFree(m_ycord);
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaFree m_ycord" << endl;
						return -1;
					}

					cuda_err = cudaFree(m_matchx);
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaFree m_matchx" << endl;
						return -1;
					}

					cuda_err = cudaFree(m_matchy);
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaFree m_matchy" << endl;
						return -1;
					}

					cuda_err = cudaFree(m_prev_xcord);
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaFree m_prev_xcord" << endl;
						return -1;
					}

					cuda_err = cudaFree(m_prev_ycord);
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaFree m_prev_ycord" << endl;
						return -1;
					}

					cuda_err = cudaFree(m_prev_matchx);
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaFree m_prev_matchx" << endl;
						return -1;
					}
					cuda_err = cudaFree(m_prev_matchy);
					if(cuda_err != cudaSuccess)
					{
						cout << "ERROR: Failed cudaFree m_prev_matchy" << endl;
						return -1;
					}

					error = cudaGetLastError();
					if (error != cudaSuccess) {
			  		fprintf(stderr, "ERROR: %s \n", cudaGetErrorString(error));
					cout<<"after freeing m_prev_matchy frame="<<frame<<endl;
					return -1;
					}




					for(int b=0; b<nkpts; b++)
					{	
						if(h_matchx[b]>720){h_matchx[b]=0;}
						if(h_matchy[b]>1280){h_matchx[b]=0;}
						if(h_prev_matchy[b]>1280){h_prev_matchy[b]=0;}
						if(h_prev_matchx[b]>720){h_prev_matchx[b]=0;}

						if(h_matchx[b]<1){h_matchx[b]=0;}
						if(h_matchy[b]<1){h_matchx[b]=0;}
						if(h_prev_matchy[b]<1){h_prev_matchy[b]=0;}
						if(h_prev_matchx[b]<1){h_prev_matchx[b]=0;}

						if(h_matchx[b] >1 && h_matchy[b] >1 && h_prev_matchy[b] >1 && h_prev_matchx[b] >1)
						{	//hiu++; flag_copy_matchcord_to_curr = 1;
							line( color_image, Point(h_matchx[b], h_matchy[b]), Point(h_prev_matchx[b],h_prev_matchy[b]), Scalar(255,0,0) );
							//cout<<h_matchx[b]<<" "<<h_matchy[b]<<" "<<h_prev_matchx[b]<<" "<<h_prev_matchy[b]<<" "<<b<<endl;
						}
					}

					memcpy(frame_match_x + (frame_count -1)*nkpts, h_matchx, nkpts*sizeof(int));
					memcpy(frame_match_y + (frame_count -1)*nkpts, h_matchy, nkpts*sizeof(int));


					if((frame%10 == 0) && frame_count==10)
					{

						memcpy(prev_x, h_prev_matchx, nkpts*sizeof(int));
						memcpy(prev_y, h_prev_matchy, nkpts*sizeof(int));
						memcpy(curr_x, h_matchx, nkpts*sizeof(int));
						memcpy(curr_y, h_matchy, nkpts*sizeof(int));
						for(int cont=0; cont<nkpts; cont++)
						{
							if(h_matchx[cont] >1 && h_matchy[cont] >1 && h_prev_matchy[cont] >1 && h_prev_matchx[cont] >1)
							{
								//cout<<prev_x[cont]<<" "<<prev_y[cont]<<" "<<h_prev_matchx[cont]<<" "<<h_prev_matchy[cont]<<" "<<endl;
								//cout<<curr_x[cont]<<" "<<curr_y[cont]<<" "<<h_matchx[cont]<<" "<<h_matchy[cont]<<endl;
							}	
						}						

					}





					free(h_matchx);
					free(h_matchy);
					free(h_prev_matchx);
					free(h_prev_matchy);
					free(gl_desc_tr);
					free(prev_x_tr);
					free(prev_y_tr);
					//free(gl_id_tr);

					frame_back--; 
					if(frame_count==10){break;}


			} /////end of while loop for matcher kernel
		
		} ///end of if frame >1

				key_id=0;
				for(int hi=0; hi<nkpts; hi++)
				{
					int bc =0;
					if(xcord[hi]>0 && ycord[hi]>0)
					{
						for(int hj=0; hj<frame_count*nkpts; hj++)
						{
							if(frame_match_x[hj]>0 && frame_match_y[hj]>0)
							{
								if( frame_match_x[hj] == xcord[hi] && frame_match_y[hj] == ycord[hi] )
								{
									bc++;
								}
							}
			
						}
			
						if(bc ==0)
						{
							unique_id++;
							gl_id[(frame*nkpts)+key_id] = unique_id;
							gl_x[(frame*nkpts)+key_id] = xcord[hi];
							gl_y[(frame*nkpts) + key_id] = ycord[hi];
							//printf("%d	%d	%d\n",gl_x[(frame*nkpts)+key_id],gl_y[(frame*nkpts) + key_id],gl_id[(frame*nkpts)+key_id]);
							//cout<<key_id<<" "<<frame<<endl;
							for(int nc=0; nc<8; nc++)
							{
								gl_desc[key_id + (frame*nkpts)].my_bits[nc] = h_my_desc[hi].my_bits[nc];
							
							}
							key_id++;

			
						}
					}

				}










		//circle(input_image, Point(200,100), 3, Scalar(0,255,0),2,0);

       		imshow("wi", color_image);
	        waitKey(10);// wait to display frame
	
		    /*char filename[128];
		   sprintf(filename, "frame_%06d.jpg", frame_count);
		   imwrite(filename, input_image);
		frame_count++;*/




////////////////////////////////serial implementation below /////////////////////////////////////////////////////////////////////////////////////////////		
		
		struct desc *my_desc = (struct desc*) malloc(nkpts*sizeof(struct desc));
		memset(my_desc,0,nkpts*sizeof(struct desc)); 
		bool k;
	
		/*for(int j=0; j<nkpts; j++)
		{
			
			if(xcord[j]!=0 && ycord[j]!=0)
			{	if(frame==0){cout<<xcord[j]<<" "<<ycord[j]<<" "<<j<<endl;}
				uint32_t gg=0;
				for(int i=0; i<8; i++)
				{
					uint32_t p_desc = 0;  
					
		
					for(int bit = 0; bit<32; bit++)
					{
						offx1 = xcord[j]+offsetx1[(32*i) + bit];
						offy1 = ycord[j]+offsety1[(32*i) + bit];
						offy2 = ycord[j]+offsety2[(32*i) + bit];
						offx2 = xcord[j]+offsetx2[(32*i) + bit];
						k = (h_in[offy1*w+offx1] < h_in[offy2*w+offx2]);
						
						p_desc= p_desc<<1; 
						p_desc = p_desc | k ;//(h_in[offy1*w+offx1] < h_in[offy2*w+offx2]);

						//p_desc |=;
						 //printf("%d", k); 
						//if(offx1>1280 || offy1>720 || offx1<1 || offy1<1 || offx2>1280 || offx2<1 ||  offy2>720 || offy2<1) 
						//{
						//	printf("%d	%d	%d	%d\n", xcord[j], offsetx2[(32*i) + bit], offx2, offy2 );
						//}

					}
					my_desc[j].my_bits[i] = p_desc; 
					if(frame==0){{printf("%u  \n", my_desc[j].my_bits[i]);}} 
					if(i>0)
					{
						gg = my_desc[j].my_bits[i]^my_desc[j].my_bits[i-1];
						if(gg>600000)
						{
							//printf("%u	", gg);
						}
					}

					

				}


				//printf("\n");
			}
			

		}

	//memcpy(gl_desc + (frame)*nkpts, my_desc, nkpts*sizeof(struct desc));

					//memcpy(curr_x, xcord, c[0]*sizeof(int));
					//memcpy(curr_y, ycord, c[0]*sizeof(int));




		int jj=0; int kb=0; int kd=0; //= 5 % 3;
		if(frame>0)
		{//int bb=(frame-1)*nkpts; bb<frame*nkpts; bb++
			for(int bb=0; bb<frame*nkpts; bb++)
			{	//printf("%d	%d	%d	%d\n", xcord[bb],ycord[bb],prev_x[bb],prev_y[bb] );
				//cout<<bb<<endl;
				//cout<<xcord[bb]<<" "<<ycord[bb]<<endl;
				kd = bb%1000;
				if(kd==0)
				{
					kb++;

					int *tmp4 = (int *) realloc(frame_match_x, nkpts*(kb)*sizeof(int));
					if(tmp4 == NULL)
					{
						printf("gl_desc realloc failed\n");
						return -1;

					}
					else
					{
						frame_match_x = tmp4;

					}


					int *tmp5 = (int *) realloc(frame_match_y, nkpts*(kb)*sizeof(int));
					if(tmp4 == NULL)
					{
						printf("gl_desc realloc failed\n");
						return -1;

					}
					else
					{
						frame_match_y = tmp5;

					}


									
			
				}
				//cout<<bb<<" "<<frame<<" "<<" "<<kd<<" "<<kb<<endl;
				for(int ss=0;ss<8;ss++)
				{

					//{cout<<gl_desc[bb].my_bits[ss]<<" "<<my_desc[bb].my_bits[ss]<<" "<<ss<<" "<<bb<<endl;}
					//{cout<<my_desc[bb].my_bits[ss]<<endl;}


				}

				int best_desc =-1;
				int best_hamming = 41;
				int best_gl_id = -1;
				for(int cc=0; cc<c[0]; cc++)  
				{	uint32_t hamming=0;

					for(int dd=0;dd<8;dd++)
					{
						hamming += __builtin_popcount(gl_desc[bb].my_bits[dd]^my_desc[cc].my_bits[dd]);
						//if(gl_desc[bb].my_bits[dd] == my_desc[cc].my_bits[dd] )
						//{cout<<gl_desc[bb].my_bits[dd]<<" "<<my_desc[cc].my_bits[dd]<<" "<<dd<<endl;}

					}
					
					if(hamming<best_hamming)
					{	
						best_desc = cc;
						best_hamming = hamming;
						//best_gl_id=gl_id[bb-(frame-1)*nkpts];
	


						//cout<< gl_id[bb-(frame-1)*nkpts]<<" "<<bb<<endl;
						

						for(int nn=0; nn<8; nn++)
						{	//if(gl_desc[bb].my_bits[nn] == my_desc[cc].my_bits[nn])
							//{cout<<gl_desc[bb].my_bits[nn]<<" "<<my_desc[cc].my_bits[nn]<<endl;}
							//printf("%d\n",30548141^30548141);

						}
						
					}

				}
				if(best_desc != -1)
				{
					//curr_match[bb-(frame-1)*nkpts] = best_gl_id;
					//curr_x[bb-(frame -1)*nkpts] = xcord[best_desc];
					//curr_y[bb-(frame-1)*nkpts] = ycord[best_desc];

					//curr_match[bb-(kb)*nkpts] = best_gl_id;
					//curr_x[bb-(kb)*nkpts] = xcord[best_desc];
					//curr_y[bb-(kb)*nkpts] = ycord[best_desc];
					//printf("%d	%d	%d	%d	%u	%d\n", xcord[best_desc], ycord[best_desc],prev_x[bb-(frame-1)*nkpts],prev_y[bb-(frame-1)*nkpts],best_hamming,frame);
					frame_match_x[bb] = xcord[best_desc];
					frame_match_y[bb] = ycord[best_desc];
					matched_id[bb] = gl_id[bb];
					//printf("%d	%d	%d	%d	%d\n", gl_x[bb], gl_y[bb] , frame_match_y[bb], frame_match_y[bb], matched_id[bb]);
			
					line( input_image, Point(ycord[best_desc], xcord[best_desc]), Point(gl_y[bb],gl_x[bb]), Scalar(255,0,0) );
					draw_flag=1;


				}
				else
				{
					//curr_match[bb-(frame-1)*nkpts] = 0;
				}



			}
		}




		key_id=0;
		for(int hi=0; hi<c[0]; hi++)
		{
			int bc =0;

			for(int hj=0; hj<frame*nkpts; hj++)
			{

				if( frame_match_x[hj] == xcord[hi] && frame_match_y[hj] == ycord[hi] )
				{
					bc++;

				}
			
			}
			
			if(bc ==0)
			{
				key_id++; unique_id++;
				gl_id[(frame*nkpts)+key_id] = unique_id;
				gl_x[(frame*nkpts)+key_id] = xcord[hi];
				gl_y[(frame*nkpts) + key_id] = ycord[hi];
				//printf("%d	%d	%d\n",gl_x[(frame*nkpts)+key_id],gl_y[(frame*nkpts) + key_id],gl_id[(frame*nkpts)+key_id]);
				for(int nc=0; nc<8; nc++)
				{
					gl_desc[(frame*nkpts) + key_id].my_bits[nc] = h_my_desc[hi].my_bits[nc];
				
				}

			
			}

		}*/



//////////////////////////////////////////////// end of serial implemetaion ////////////////////////////////////////////////////////////////////////////////






		free(c);
		free(xcord);
		free(ycord);
		free(h_outptr);
		free(h_doutptr);
	   	free(h_in);
		free(h_my_desc);
		free(my_desc);
		
		free(frame_match_x);
		free(frame_match_y);






	}////end of for loop

	/*int curr_sumx=0, curr_sumy=0, prev_sumx=0, prev_sumy=0 ;
	int currx_tr[8];
	int curry_tr[8];
	int prevx_tr[8];
	int prevy_tr[8];
	//for(int hh = 0; hh<10; hh++)
	{
		for(int kk=0; kk<8; kk++)
		{
			label:  int v = rand() % (5000 + 1 - 0) + 0; 
				if(curr_x[v]==0){goto label; }
				currx_tr[kk] = curr_x[v];
				curry_tr[kk] = curr_y[v];
				prevx_tr[kk] = prev_x[v];
				prevy_tr[kk] = prev_y[v];
				//cout<<curr_x[v]<<" "<<curr_y[v]<<" "<<prev_x[v]<<' '<<prev_y[v]<<endl;
				//line( color_image, Point(curr_x[v], curr_y[v]), Point(prev_x[v],prev_y[v]), Scalar(255,0,0) );
				curr_sumx = curr_sumx + curr_x[v]; curr_sumy = curr_sumy + curr_y[v]; prev_sumx = prev_sumx + prev_x[v]; prev_sumy = prev_sumy + prev_y[v];
				//cout<<curr_sumx<<" "<<curr_x[v]<<endl;


		}

		//printf("\n");
	}


//int curr_centroidx = curr_sumx/8;
//int curr_centroidy = curr_sumy/8;
//int prev_centroidx = prev_sumx/8;
//int prev_centroidy = prev_sumy/8;


double curr_d=0, prev_d=0;

//cout<<"sample"<<" "<<"centroid"<<" "<<" diff"<<endl;
for (int vv =0; vv<8; vv++)
{
	int diff_curr_x = currx_tr[vv] - curr_centroidx; int diff_curr_y = curry_tr[vv] - curr_centroidy;
	//cout<<currx_tr[vv]<<" 	"<<curr_centroidx<<"	 "<<diff_curr_x<<endl;
	int diff_curr_x_squared  = diff_curr_x* diff_curr_x; int diff_curr_y_squared = diff_curr_y * diff_curr_y;
	curr_d = sqrt((double)diff_curr_x_squared + (double)diff_curr_y_squared) + curr_d;

	int diff_prev_x = prevx_tr[vv] - prev_centroidx; int diff_prev_y = prevy_tr[vv] - prev_centroidy;
	int diff_prev_x_squared  = diff_prev_x * diff_prev_x; int diff_prev_y_squared = diff_prev_y * diff_prev_y;
	prev_d = sqrt((double)diff_prev_x_squared + (double)diff_prev_y_squared) + prev_d;



}	

//double prev_dist_to_centroid = prev_d/8;
//double curr_dist_to_centroid = curr_d/8;

//cout<<" centroid"<<endl;
//cout<<curr_centroidx<<" "<<curr_centroidy<<" "<<prev_centroidx<<" "<<prev_centroidy<<endl;
//cout<<prev_dist_to_centroid<<" "<<curr_dist_to_centroid<<endl;

double leftx[8] ={0};
double lefty[8] ={0};
double rightx[8] ={0};
double righty[8] ={0};

//cout<<"normalized"<<endl;
//cout<<"normalized_leftx"<<"  "<<"original_left_x"<<"  "<<"centroid_x"<<endl;

for(int yy=0; yy<8; yy++)
{

	leftx[yy] = (double)(currx_tr[yy] - curr_centroidx)/curr_dist_to_centroid ; lefty[yy] = (double)(curry_tr[yy] - curr_centroidy)/curr_dist_to_centroid;
	rightx[yy] = (double)( prevx_tr[yy] - prev_centroidx )/prev_dist_to_centroid;  righty[yy] = (double)( prevy_tr[yy] - prev_centroidy )/prev_dist_to_centroid; 
	//cout<<leftx[yy]<<" 		"<<currx_tr[yy]<<"		 "<<curr_centroidx<<endl;
	//cout<<leftx[yy]<<" "<<lefty[yy]<<" "<<rightx[yy]<<' '<<righty[yy]<< endl;

}*/



	double *best_cam_model = (double *) malloc(sizeof(double)*9);
	double best_score = 3;
	double model_score= 3;

	for(int trials =0; trials<40; trials++)
	{
		double *leftx = (double *) malloc(sizeof(double)*8);
		double *lefty = (double *) malloc(sizeof(double)*8);
		double *rightx = (double *) malloc(sizeof(double)*8);
		double *righty = (double *) malloc(sizeof(double)*8);

		double *trans_mat = (double *) malloc(sizeof(double)*6);

		normalize_coordinates( curr_x, curr_y, prev_x, prev_y, leftx, rightx, lefty, righty, trans_mat );



		double *A = (double *) malloc(sizeof(double)*8*9);
		double *unit_fund_mat = (double *) malloc(sizeof(double)*9);
		double *fund_cam_mat = (double *) malloc(sizeof(double)*9);
		matrix_A(A, leftx, rightx, lefty, righty);

		double *left_H = (double *) malloc(sizeof(double)*9);
		double *right_H = (double *) malloc(sizeof(double)*9);

		for(int bla =0; bla<6; bla++)
		{cout<<" trans_mat:"<<trans_mat[bla]<<endl;}


		left_H[0]= 1/trans_mat[2]; left_H[1] = 0; left_H[2] = 0;
		left_H[3]= 0 ; left_H[4]= 1/trans_mat[2] ; left_H[5]= 0; left_H[6]= -trans_mat[0]/trans_mat[2]; left_H[7]=-trans_mat[1]/trans_mat[2] ; left_H[8]=1 ;

		right_H[0]= 1/trans_mat[5]; right_H[1] = 0; right_H[2] = 0;
		right_H[3]= 0 ; right_H[4]= 1/trans_mat[5] ; right_H[5]= 0; right_H[6]= -trans_mat[3]/trans_mat[5]; right_H[7]=-trans_mat[4]/trans_mat[5] ; right_H[8]=1 ;




		/*cout<<"left_H:"<<endl;

		for(int mm=0; mm<3; mm++)
		{

			for(int nn=0; nn<3; nn++)
			{
				cout<<left_H[nn*3+mm]<<" \t";
	
			}
			printf("\n");

		}*/

		transpose(right_H, 3, 3);

		/*cout<<" transpose right_H:"<<endl;

		for(int xx=0; xx<3; xx++)
		{

			for(int yy=0; yy<3; yy++)
			{
				cout<<right_H[yy*3+xx]<<" \t ";
	
			}
			printf("\n");

		}*/



		svd_dd(A, 8, 9,unit_fund_mat);
		svd_fund(unit_fund_mat,right_H,left_H, fund_cam_mat);

		model_score = total_error (curr_x, curr_y, prev_x, prev_y, nkpts, fund_cam_mat);

		if(abs(best_score) < abs(model_score))
		{
			best_score = model_score;
			memcpy(best_cam_model,fund_cam_mat, sizeof(double)*3*3);

		}



		free(A);
		free(leftx);
		free(rightx);
		free(lefty);
		free(righty);
		free(unit_fund_mat);
		free(left_H);
		free(right_H);
		free(fund_cam_mat);
		free(trans_mat);
	} //end of camerA model for loop
	cout<<"best score:"<<best_score<<endl;
	cout<<"best_cam_model: "<<endl;
	for(int oo=0; oo<3; oo++)
	{
		for(int pp=0; pp<3; pp++)
		{
			cout<<best_cam_model[pp*3+oo]<<endl;
		}

	}

	free(best_cam_model);









//imshow("wi", color_image);
//waitKey(10000);
	




	free(curr_x);
	free(curr_y);
	free(prev_x);
	free(prev_y);
	free(prev_match);
	free(curr_match);

	free(gl_desc);
	free(gl_x);
	free(gl_y);
	free(gl_id);
	free(matched_id);



	//free(see);

   	return 0;

	

}  /// end of main function