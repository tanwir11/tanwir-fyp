

#include <cuda_runtime_api.h>
#include <cuda.h>
#include <stdint.h>

struct desc {
 uint32_t my_bits[8];
};

extern "C" void cuda_descriptor(unsigned char *ptr, int *xcord, int *ycord, int w, desc *my_desc, dim3 gridsize, dim3 blocksize);
