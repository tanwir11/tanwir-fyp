////////////////////////////////////////    startting of fast9 kernel   ///////////////////////////////////////////////////////////////

extern const int nkpts = 5000;
#include <stdio.h>

__global__ void iscorner(unsigned char *inptr, int *val, int *xcord, int *ycord, int w, int h, unsigned char *outptr, unsigned char *doutptr)
{

  int offset[16][2] = {{3,0},{3,1},{2,2},{1,3},{0,3},{-1,3},{-2,2},{-3,1},{-3,0},{-3,-1},{-2,-2},{-1,-3},{0,-3},{1,-3},{2,-2},{3,-1}};
  int thresh=0;
  int p=0; //variable for finding bright corner
  int q=0; //variable for finding dark corner
  int pt; //counter for traversing offsets
  int my_index=0; 
  


  int x =  blockIdx.x*blockDim.x + threadIdx.x; //counters for traversing rows
  int y =  blockIdx.y*blockDim.y + threadIdx.y; //counters for traversing columns
  int index =  y*w + x;
 // printf("%d	%d\n",y,x);
	if(index<=(h*w)){
	 // printf("%d	%d\n",y,x);
	  outptr[index] = 0;
		//printf("%d\n",(int)inptr[index]);
		if(y>129 && y<(h-130) && x>129 && x<(w-130)){
			//printf("%d	%d\n",y,x);
			//printf("%d\n",index);
			for(pt=0;pt<32;pt++){

				
				thresh = (int)inptr[index] - (int)inptr[(y+offset[pt%16][1])*w + (x+offset[pt%16][0])];
				//printf("%d	%d\n",offset[pt][1],offset[pt][0]);
				if(thresh>60){p=p+1;}
				else{p=0;}
				
				if(thresh<-60){q=q+1;}
				else{q=0;}
				
				if(p>=9){	
					//p=0;
					//my_index = atomicAdd(val,1);
					//xcord[index] = x;
					//ycord[index] = y;

					my_index = atomicAdd(val,1);
					if(my_index<nkpts){
						xcord[my_index] = x;
						ycord[my_index] = y;		
						outptr[index] = inptr[index];
						}
					__threadfence();
					//printf("%d	%d	p = %d\n",y,x,p); 
					p=0;
				
				 } // end of if statement for checking and storing BRIGHT corner pixel				

				if(q>=9){	
					//q=0;
					//my_index = atomicAdd(val,1);
					//xcord[x] = x;
					//ycord[y] = y;

					my_index = atomicAdd(val,1);
					if(my_index<nkpts){
						xcord[my_index] = x;
						ycord[my_index] = y;				
						doutptr[index] = inptr[index];
						}
					__threadfence();
					//printf("%d	%d	q = %d\n",y,x,q); 
					q=0;
				 } // end of if statement for checking and storing DARK corner pixel

				
			} // end of circular traversing for loop


		} /// end of if statement for circular traversal


	} ///end of if statement to ensure that number of pixels is not exceeded


}  ///// end of fast9 kernel


extern "C" void cuda_iscorner(unsigned char *inptr, int *val, int *xcord, int *ycord, int w, int h, unsigned char *outptr, unsigned char *doutptr, dim3 gridsize, dim3 blocksize)
{
			
			iscorner <<< gridsize, blocksize >>> (inptr, val, xcord, ycord, w, h, outptr, doutptr);

}





