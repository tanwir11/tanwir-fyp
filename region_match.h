

#include <cuda_runtime_api.h>
#include <cuda.h>


extern "C" void cuda_region_match( desc *old_desc, desc *new_desc, int *xcord,  int *ycord, int *matchx,  int *matchy, int *prev_x, int *prev_y, int *prev_matchx, int *prev_matchy, int h , int w , dim3 gridsize, dim3 blocksize);