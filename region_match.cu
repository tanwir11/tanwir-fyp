

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

const int nkpts = 5000;

struct desc {
unsigned int my_bits[8];
};

/*__global__
void region_match(int *xcord, int *x1cord, int *ycord, int *y1cord, desc *old_desc, desc *new_desc, int *matchx, int *matchx1, int *matchy, int *matchy1, int h , int w, int *idx, int *idx2)
{

	int x =  blockIdx.x*blockDim.x + threadIdx.x; //counters for traversing rows
	int y =  blockIdx.y*blockDim.y + threadIdx.y; //counters for traversing columns
	int index =  y*w + x; //number of thread being executed
	int region =  0.25*sqrtf(( w*w ) + ( h*h ))  ;   //threshold region of matching descriptors
	int dist = 0; // varaiable for euclidean distance between old and new keypoints initialized to 0
	int l = 0; //variable for counting difference between descriptors
	int k = 0; //variable for finding difference in the variables

	if(index<nkpts){
		
		//printf("%d\n",index);
		//if (index == 91){
		if((ycord[index] && xcord[index])!=0)
		{	
			//printf("%d\n",index);

			for(int i=0; i<nkpts; i++)
			{ 
				
				//printf("%d\n",l);
				if((y1cord[i] && x1cord[i])!=0)
				{
	   		     		dist = (int) ( sqrtf( (xcord[index] - x1cord[i])*(xcord[index] - x1cord[i]) + (ycord[index] -y1cord[i])*(ycord[index] -y1cord[i]) ) );
					//printf("%d	%d	%d	%d\n", xcord[index], ycord[index], x1cord[i], y1cord[i] );
					//printf("%d	%d\n", region, dist);

					
					
						//printf("%d	%d\n", x1cord[i], y1cord[i]);
						for(int j=0; j<256; j++)
						{	l=0; 
							k = (int) (old_desc[index].my_bits[j] ^ new_desc[i].my_bits[j]);
							//printf("%d	%d	%d\n", old_desc[index].my_bits[j], new_desc[i].my_bits[j], k );
							if(k==1)
							{	//matchx[index] = 0; matchy[index] = 0;
								l++;  break;
							} // hamming distance more than 1, therefore break away from the loop
						} //xor for loop
						//printf("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ \n");
						if(l==0)
						{
							matchx[index] = xcord[index]; matchy[index] = ycord[index];  matchx1[index] = x1cord[i]; matchy1[index] = y1cord[i];
							idx[index] = index; idx2[index] = i;
							printf("%d	%d 	%d	%d\n", matchx[index], matchy[index], idx[index], idx2[index]); 
							//printf("%d	%d	%d	%d\n", matchx[index], matchy[index], matchx1[index], matchy1[index]);		
						} // end o//f storing the matches into arrays	

				
 					
				} // end of checking for non-zero x and y cord
				
			} // end of big for loop

		} // end of checking for non-zero x and y cord
		

	} // end of index checking

}



extern "C" void cuda_region_match(int *xcord, int *x1cord, int *ycord, int *y1cord, desc *old_desc, desc *new_desc, int *matchx, int *matchx1, int *matchy, int *matchy1, int h , int w, int *idx, int *idx2, dim3 gridsize, dim3 blocksize)
{
	region_match<<<gridsize,blocksize>>>(xcord, x1cord, ycord, y1cord, old_desc, new_desc, matchx, matchx1, matchy, matchy1, h, w, idx, idx2);

}*/












/*__global__
void region_match(desc *old_desc, desc *new_desc, int *gl_id_tr, int *gl_id_new, int *xcord,  int *ycord, int *matchx,  int *matchy, int *idx, int h , int w)
{

	int x =  blockIdx.x*blockDim.x + threadIdx.x; //counters for traversing rows
	int y =  blockIdx.y*blockDim.y + threadIdx.y; //counters for traversing columns
	int index =  y*w + x; //number of thread being executed
	int region =  0.25*sqrtf(( w*w ) + ( h*h ))  ;   //threshold region of matching descriptors
	int dist = 0; // varaiable for euclidean distance between old and new keypoints initialized to 0
	int l = 0; //variable for counting difference between descriptors
	int k = 0; //variable for finding difference in the variables

	
	if(index<(nkpts))
	{
		if(gl_id_tr[index] != 0)
		{	//printf("%d %d %d\n",x_frame[index],y_frame[index],gl_id_tr[index] );
			
			for(int i=0; i<nkpts; i++)
			{	//printf("%d %d\n",xcord[i],ycord[i] );
				if(xcord[i] && ycord[i] != 0)
				{		//printf("%d %d\n",xcord[i],ycord[i] );
						l=0;
						for(int j=0; j<256; j++)
						{	 
							k = (int) (old_desc[index].my_bits[j] ^ new_desc[i].my_bits[j]);
							
							//printf("%d	%d	 %d\n", old_desc[index].my_bits[j], new_desc[i].my_bits[j], k );
							if(k==1)
							{
					        		l++;  break;
							} // hamming distance more than 1, therefore break away from the loop
						} //xor for loop
						//printf("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ \n");
						if(l<2)
						{
							gl_id_new[index] = gl_id_tr[index];
							matchx[index] = xcord[i]; 
							matchy[index] = ycord[i]; 
							idx[index] = index; 
							

							//printf("%d	%d	%d	%d\n", matchx[index], matchy[index], gl_id_new[index], idx[index]); 
									
						} // end o////f storing the matches into arrays	
						else
						{	gl_id_new[index] = 0; //printf("%d\n", gl_id_new[index]);
							matchx[index] = 0; matchy[index] = 0;
	
						}


				}
	
			}
		
		}
		
	//printf("%d\n", index);
	}///// end of indexing threads






}///// end of kernel


extern "C" void cuda_region_match(desc *old_desc, desc *new_desc, int *gl_id_tr, int *gl_id_new, int *xcord,  int *ycord, int *matchx, int *matchy, int *idx, int h , int w ,  dim3 gridsize, dim3 blocksize)
{
	region_match<<<gridsize,blocksize>>> (old_desc, new_desc, gl_id_tr, gl_id_new, xcord, ycord, matchx, matchy, idx, h, w);
 
}*/













__global__
void region_match(desc *old_desc, desc *new_desc, int *xcord,  int *ycord, int *matchx,  int *matchy, int *prev_x, int *prev_y, int *prev_matchx, int *prev_matchy, int h , int w)
{

	int index =  blockIdx.x*blockDim.x + threadIdx.x; //counters for traversing rows

	//int region =  0.25*sqrtf(( w*w ) + ( h*h ))  ;   //threshold region of matching descriptors
	//int dist = 0; // varaiable for euclidean distance between old and new keypoints initialized to 0
	//int l = 0; //variable for counting difference between descriptors
	//int k = 0; //variable for finding difference in the variables
	//int my_index =0;

	
	if(index<(nkpts))
	{
		if(prev_x[index]!=0 && prev_y[index]!=0)
		{
			int best_desc =-1;
			int best_hamming = 41;
			//int best_gl_id = -1;

			for(int i=0; i<nkpts; i++)
			{	//printf("%d %d\n",xcord[i],ycord[i] );
				if(xcord[i] && ycord[i] != 0)
				{		//printf("%d %d\n",xcord[i],ycord[i] );
						uint32_t hamming=0;
						for(int j=0; j<8; j++)
						{	 
							hamming +=  __popc(old_desc[index].my_bits[j] ^ new_desc[i].my_bits[j]);
							

						} //xor for loop
						//printf("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ \n");
						if(hamming<best_hamming)
						{

							best_hamming = hamming;
							best_desc = i;
						}
							//gl_id_new[index] = gl_id_tr[index];
						if(best_desc!=-1)
						{
							//my_index = atomicAdd(m_count,1);
							matchx[index] = xcord[best_desc]; 
							matchy[index] = ycord[best_desc];
							prev_matchx[index] = prev_x[index];
							prev_matchy[index] = prev_y[index];
							

							//printf("%d	%d	%d	%d	%d\n", matchx[index], matchy[index], prev_matchx[index], prev_matchy[index], best_hamming ); 
							//matched_id[my_index] = m_id[index];
						}
						else
						{
							matchx[index] = 0; 
							matchy[index] =0;
							prev_matchx[index] = 0;
							prev_matchy[index] = 0;

						}
						//printf("%d	%d	%d	%d	%d	%d\n", matchx[index], matchy[index], prev_matchx[index], prev_matchy[index], best_hamming, index ); 
							
							
 
									
						 // end o////f storing the matches into arrays	
						/*else
						{	gl_id_new[index] = 0; //printf("%d\n", gl_id_new[index]);
							matchx[index] = 0; matchy[index] = 0;
	
						}*/


				}
	
			}
		

		}		
	//printf("%d\n", index);
	}///// end of indexing threads






}///// end of kernel


extern "C" void cuda_region_match( desc *old_desc, desc *new_desc, int *xcord,  int *ycord, int *matchx,  int *matchy, int *prev_x, int *prev_y, int *prev_matchx, int *prev_matchy, int h , int w , dim3 gridsize, dim3 blocksize)
{
	region_match<<<gridsize,blocksize>>> (old_desc, new_desc,xcord, ycord, matchx, matchy, prev_x, prev_y, prev_matchx, prev_matchy, h, w);
 
}




