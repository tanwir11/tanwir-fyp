extern "C" void svd_fund(double *fund, double *right_H, double* left_H, double *cam_out);
extern "C" void homo(int *A,int x,int y);
extern "C" void mat_mult(double *A, double *B, int a_m, int a_n, int b_m, int b_n, double *C );
extern "C" void transpose(double *array, int w, int h);
extern "C" double total_error (int *leftx, int *lefty, int *rightx, int *righty, int nkpts, double *cam_fund_mat);
